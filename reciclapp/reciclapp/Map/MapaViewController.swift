//
//  MapaViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/6/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import FirebaseDatabase
import GoogleMaps
import GooglePlaces

class MapaViewController: UIViewController{
//AIzaSyATJUcmWfi1oX3muNQUFQGR2HUNG-nYI2Y
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0

    // An array to hold the list of recycling places
    var likelyPlaces: [GMSPlace] = []

    // The currently selected place.
    var selectedPlace: GMSPlace?

    // A default location to use when location permission is not granted.
    //let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Initialize the location manager.
//        locationManager = CLLocationManager()
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.requestAlwaysAuthorization()
//        locationManager.distanceFilter = 50
//        locationManager.startUpdatingLocation()
//        locationManager.delegate = self
//
//        placesClient = GMSPlacesClient.shared()
//    }
    
    override func loadView() {
        
        
        // Create a GMSCameraPosition that tells the map to display the
           // coordinate -33.86,151.20 at zoom level 6.
           let camera = GMSCameraPosition.camera(withLatitude: 4.603107, longitude: -74.065212, zoom: 18.0)
           let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
           //Allows to have zoom and other gestures for using the map
        mapView.settings.scrollGestures = true
        mapView.settings.zoomGestures = true
           // Establece en la vista 
           view = mapView
        

           // Creates a marker in the center of the map.
           let marker = GMSMarker()
           marker.position = CLLocationCoordinate2D(latitude: 4.603107, longitude: -74.065212)
           marker.title = "Universidad de los Andes"
           marker.snippet = "Colombia"
           marker.map = mapView
          // Enables asking for the location
           mapView.isMyLocationEnabled = true
           mapView.settings.myLocationButton = true
          //Create the markers for the centers
//           centersNear()
    }
    func centersNear(){
//        let ref = Database.database().reference()
//        ref.child("centros").observeSingleEvent(of:.value){ (snapshot) in
//            if snapshot.exists(){
//                if let location = snapshot.value as? [String: Any]{
//                    for eachLocation in location {
//                        print ("Location: \(eachLocation)")
//                        if let locationCoordinate = eachLocation.value as? [String : Any]{
//                            if let centerlatitude = locationCoordinate ["latitud"] as? Double{
//                                if let centerLongitud = locationCoordinate ["longitud"] as? Double {
//                                    print(centerlatitude)
//                                    print(centerLongitud)
//                                }
//                            }
//                        }
//                    }
//
//                }
//            }
//
//        }
        
    }
 }

//}
