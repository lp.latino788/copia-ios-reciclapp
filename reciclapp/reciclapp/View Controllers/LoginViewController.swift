//
//  LoginViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/4/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var gmailBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        Login Firebase
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
        setupElements()
    }
    func setupElements(){
        // Dissapear error label
        errorLabel.alpha=0
    }
    @IBAction func loginTapped(_ sender: Any) {
        //Signs in the user in Firebase
        guard
          let email = emailTextField.text,
          let password = passwordTextField.text,
          email.count > 0,
          password.count > 0
          else {
            return
        }

        Auth.auth().signIn(withEmail: email, password: password) { user, error in
          if let error = error, user == nil {
            let alert = UIAlertController(title: "Sign In Failed",
                                          message: error.localizedDescription,
                                          preferredStyle: .alert)
            self.showError(mensaje: "Sign in Falló")
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
          }
        }
        
        //Sends to the Home Screen
    }
    
    func showError(mensaje : String){
        errorLabel.text = mensaje
        errorLabel.alpha = 1
    }
//     // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
//         self.transicionAlHome()
    }

    @IBAction func fbLoginTapped(_ sender: Any) {
    }
    @IBAction func gmailLoginTapped(_ sender: Any) {
    }
    
}
