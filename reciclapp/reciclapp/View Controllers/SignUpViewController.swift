//
//  SignUpViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/4/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
//import GoogleSignIn
import FirebaseDatabase


class SignUpViewController: UIViewController {

    // [START Outlets]
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmationPassTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var genderField: UISegmentedControl!
    @IBOutlet weak var birthDateField: UIDatePicker!
    @IBOutlet weak var errorLabel: UILabel!
    var cleanDate = ""
    var cleanGender = "Masculino"
    
    // [END Outlets]
    
    // [START viewdidload]
    override func viewDidLoad() {
          super.viewDidLoad()
          setUpElements()
        
        //Make tabs bigger and with the blue color
        //self.navigationController?.navigationBar.prefersLargeTitles = true
        
          //GIDSignIn.sharedInstance()?.presentingViewController = self
          // Automatically sign in the user.
          //GIDSignIn.sharedInstance()?.restorePreviousSignIn()

        }
    // check the gender selected
    @IBAction func cambioGenero(_ sender: Any) {
        switch genderField.selectedSegmentIndex
        {
        case 0:
            cleanGender = "Masculino"
        case 1:
            cleanGender = "Femenino"
        case 2:
            cleanGender = "Otro"
        default:
            break
        }
    }
    
    @IBAction func cambioFecha(_ sender: Any) {
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "YYYY-MM-DD"
//       cleanDate = stringFromDate(birthDateField)
        cleanDate = "1996-02-27"
        
    }
    
    //Check the fields and validates if the input data is correct. In case something is wrong, returns error message else returns nil.
    func validateFields() -> String?{
        //Checks that all fields are filled
        if nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || confirmationPassTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Por favor llene todos los campos."
        }
        else {
        //Validates Password
        let cleanPassword = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let cleanConfirmation = confirmationPassTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if Utilities.isPasswordValid(cleanPassword!) == false {
                return "La contraseña debe tener mínimo 8 caracteres, un número y un caracter especial (Ej: $%&#@!)."
            }
            else if (cleanPassword?.isEqual(cleanConfirmation))! {
                 return "Las contraseñas no coinciden "
            }
            
        
        }
        return nil
    }
    
    func setUpElements(){
        // Dissapear error label
        errorLabel.alpha = 0
        //Give style to the text fields in the view        
        //
        
    }
        
       
    @IBAction func signUpTapped(_ sender: Any) {
        //Validate Fields
        let error = validateFields()
        if error != nil{
            //Error on any of the fields
            showErrorMessage(message: error!)
        }
        else{
          //Create the user
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { user, error in
                     if error == nil {
                       let ref = Database.database().reference()
                        let emailText = self.emailTextField.text
                        ref.child("usuarios/" + self.emailTextField.text!).setValue(["email": emailText, "nombre": self.nameTextField.text,"genero":self.cleanGender, "nacimiento":self.cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ])
//                        ref.childByAutoId().setValue(["email": emailTextField.text , "nombre": nameTextField.text,"genero":cleanGender, "nacimiento":cleanDate, "puntosActuales": "0", "puntosAcumulados" :"0" ])
                     }
                   }
          //Go to Home Screen
        }
        
        
    }
    
    //Makes error label visible and assignes the error
    func showErrorMessage( message:String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
}
