//
//  CentersViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/10/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces

class CentersViewController: UIViewController {
    // An array to hold the list of possible locations.
    var centerPlaces: [GMSPlace] = []
    var selectedPlace: GMSPlace?
    
}
