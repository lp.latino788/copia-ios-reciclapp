//
//  Tips.swift
//  reciclapp
//
//  Created by Paola Latino on 10/9/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//
import Foundation
import UIKit

class Tips: NSObject, NSCoding {
    var tip : String!
    
    //MARK: Types
    struct PropertyKey{
        static let tip = "dayTip"
    }
    //MARK: Archiving Paths
    
//    static let tip = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
//    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("routines2")
    
    init(inputTip: String) {
        self.tip = inputTip
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(tip, forKey: PropertyKey.tip)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let tip = aDecoder.decodeObject(forKey: PropertyKey.tip) as! String
        self.init(inputTip: tip)
    }
}
